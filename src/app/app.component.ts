import { Component } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { UserService } from './_services/user-service.service';
import { AlertComponent } from './_directives/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(private userService: UserService){}

  logout(){
    if(localStorage.getItem('token') != null){
      this.userService.logout();
      location.reload();
    }
  }

}
