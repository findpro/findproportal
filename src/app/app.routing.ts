import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { SettingsComponent } from './settings/settings.component';
import { ServicesComponent} from './services/services.component';
import { VideosComponent } from './videos/videos.component';
import { BigEventsComponent } from './big-events/big-events.component';


import { AuthGuard } from './_services/auth.guard';


const appRoutes: Routes = [
    { path: '', component: LoginComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard], 
        children:[
            { path: 'users', component: UsersComponent },
            { path: 'services', component: ServicesComponent }, 
            { path: 'videos', component: VideosComponent}, 
            { path: 'bigEvents', component: BigEventsComponent }
            ]
    
    },
    { path: 'settings', component: SettingsComponent },
    //{ path: 'home', component: HomeComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);