import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from '../_services/index';
import { AlertService } from '../_services/index';

import { Service } from '../_models/index';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServicesComponent implements OnInit {
  public services = [];
  public loading = true;
  constructor(private service: ServiceService, private alert: AlertService) {
    this.service.getServices()
                .subscribe((data) =>{
                  this.services = data;
                  this.loading = false;
                }, (error) =>{
                  this.alert.error(error);
                })
   }

  ngOnInit() {
  }

  disapproveService(service){
    this.service.disapprove(service._id)
            .subscribe(data => {
            this.alert.success("Success: Service " + service._id + " has been rejected!");
        },error => {
            this.alert.error(error.message);
            console.log(error);
        })
  }

}
