import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigEventsComponent } from './big-events.component';

describe('BigEventsComponent', () => {
  let component: BigEventsComponent;
  let fixture: ComponentFixture<BigEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
