import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-big-events',
  templateUrl: './big-events.component.html',
  styleUrls: ['./big-events.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BigEventsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
