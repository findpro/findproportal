export class Service{
    public _id: string;
    public ownerId: string;
    public id: string;
    public introduction: string;
    public imageURL: string;
    public price: number;
    public currency: string;
    public rateAvg: number;
    public reviewNumber: number;
}