export class User{
    public _id: string;
    public id: string;
    public nickName: string;
    public imageURL: string;
    public userLevel: number;

}