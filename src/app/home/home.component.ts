import { Component } from '@angular/core';
import { RouterLinkActive } from '@angular/router'

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent{
    public tabs = [];
    public selected: any;
    constructor(){
        this.tabs = [
        "Users",
        "Videos", 
        "Services", 
        "BigEvents"
    ];
}

  select(item) {
      this.selected = item; 
  };
  isActive(item) {
      return this.selected === item;
  };
}