import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../_services/user-service.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  public name: string;
  public password: string;
  public returnUrl: string;

  constructor(private userService: UserService,private route: ActivatedRoute,
        private router: Router, private alert: AlertService) { }

  ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home/users';
  }

  login(){
    this.userService.verifyLogin(this.name, this.password)
    .subscribe(res => {
      if(res['userLevel'] == 99){
          localStorage.setItem('id', this.name);
          localStorage.setItem('token', res['token']);
          this.router.navigate([this.returnUrl]);
      }else{
        this.alert.error("You are not authorized.");
      }

    }, error => {
      
    });
  }

}
