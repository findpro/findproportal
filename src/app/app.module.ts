import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerModule } from 'angular2-spinner';

import {DataTableModule} from "angular2-datatable";
import { routing } from './app.routing';
import { UserService } from './_services/index';
import { AlertService } from './_services/index';
import { AuthGuard } from './_services/auth.guard';
import { ServiceService } from './_services/index';



import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { AlertComponent } from './_directives/index';
import { ServicesComponent } from './services/services.component';
import { VideosComponent } from './videos/videos.component';
import { BigEventsComponent } from './big-events/big-events.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent, 
    HomeComponent, 
    SettingsComponent, 
    UsersComponent,
    AlertComponent,
    ServicesComponent,
    VideosComponent,
    BigEventsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule, 
    routing, 
    SpinnerModule,
    DataTableModule
  ],
  providers: [UserService, 
              AlertService,
              AuthGuard, 
              ServiceService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
