import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';

import { baseURL } from '../app.config';
import { User } from '../_models/user';

@Injectable()
export class UserService {
  public data = {'id': null, 'password': null};
  constructor(private http: HttpClient ) { 
  }

  verifyLogin(name, password){
      this.data.id = name;
      this.data.password = password;
      return this.http.post(baseURL + '/user/login', this.data);

  }
  logout(){
      // remove user from local storage to log user out
        localStorage.removeItem('id');
        localStorage.removeItem('token');
  }

  //Get all the users
  getUsers(){
      let header: HttpHeaders = new HttpHeaders({'sc-token': 'token', 'sc-username': 'id', 
                                              'x-sc-token': localStorage.getItem('token'), 
                                              'x-sc-username': localStorage.getItem('id')});
      return this.http.get(baseURL + '/admin/user', {headers: header})
                      .map((data: Array<any>) =>{
                          let result: Array<User> = [];
                          if(data){
                            data.forEach((element) => {
                                let user = new User();
                                user._id = element['_id'];
                                user.id = element['id'];
                                user.imageURL = element['imageURL'];
                                user.nickName = element['nickname'];
                                user.userLevel = element['userLevel'];
                                result.push(user);
                            })
                          }
                          return result;    
                      })
                                                           

  }

  //Approve the user to be the service provider
  approveApplyForPro(user){
      if(user == null){
          return;
      }
      let header: HttpHeaders = new HttpHeaders({'sc-token': 'token', 'sc-username': 'id', 
                                              'x-sc-token': localStorage.getItem('token'), 
                                              'x-sc-username': localStorage.getItem('id')});
      let body = {"_id": user._id};
      return this.http.post(baseURL + "/user/approveApplyForPro", body, {headers: header});
  }

  //Reject the user to be the service provider
  rejectApplyForPro(user){
      if(user == null){
          return;
      }
      let header: HttpHeaders = new HttpHeaders({'sc-token': 'token', 'sc-username': 'id', 
                                              'x-sc-token': localStorage.getItem('token'), 
                                              'x-sc-username': localStorage.getItem('id')});
      let body = {"_id": user._id};
      return this.http.post(baseURL + "/admin/rejectApplyForPro", {body: body}, {headers: header});
  }
}
