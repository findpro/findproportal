import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';

import { baseURL } from '../app.config';
import { Service } from '../_models/service';

@Injectable()
export class ServiceService {

  constructor(private http: HttpClient) { }


  getServices(){
    return this.http.get(baseURL + "/service")
                    .map((data: Array<any>) =>{
                          let result: Array<Service> = [];
                          if(data){
                            data.forEach((element) => {
                                let service = new Service();
                                service._id = element['_id'];
                                service.id = element['id'];
                                service.imageURL = element['imageURL'];
                                service.ownerId = element['ownerId'];
                                service.introduction = element['introduction'];
                                result.push(service);
                            })
                          }
                          return result;    
                      });
  }

  disapprove(serviceId){
    if(serviceId == null){
          return;
      }
      let header: HttpHeaders = new HttpHeaders({'sc-token': 'token', 'sc-username': 'id', 
                                              'x-sc-token': localStorage.getItem('token'), 
                                              'x-sc-username': localStorage.getItem('id')});
      let body = {"serviceId": serviceId};
      return this.http.post(baseURL + "/admin/service/disapprove", body, {headers: header});
  }
}
 