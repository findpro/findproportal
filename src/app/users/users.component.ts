import { Component, OnInit, Directive} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../_services/user-service.service';
import { AlertService } from '../_services/index';

import { User } from '../_models/user';

@Component({
    selector: 'users',
    styleUrls: ['./users.component.scss'],
    templateUrl: 'users.component.html'
    
})

export class UsersComponent{
    public users = [];
    public loading = true;
    constructor(private userService: UserService, private router: Router, private alert: AlertService){
        this.userService.getUsers()
        .subscribe((data) => {
            this.users = data;
            this.loading = false;
            console.log(this.users[0]);
        }, error => {
            if(error.status == 401){
                this.router.navigate(['/login']);
            }
            console.log(error);
        });
    }

    OnInit(){
        
    }

    approveApplyForPro(user){
        this.userService.approveApplyForPro(user)
        .subscribe(data => {
            this.alert.success("Success: user " + user.name + " has been a service provider!");
        },error => {
            this.alert.error(error.message);
            console.log(error);
        })
    }

    rejectApplyForPro(user){
        this.userService.rejectApplyForPro(user)
        .subscribe(data => {
            this.alert.success("Success: user " + user.name + " has been rejected to be a service provider!");
        },error => {
            this.alert.error(error.message);
            console.log(error);
        })
    }


}